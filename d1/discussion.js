/*
	What is a client?

	A client is an application which creates requests for RESOURCES from a server. A client will trigger an action, in the web development context, through a URL and wait for the response of the server

	What is a server?
	
	A server is able to host and deliver RESOURCES requested by a client.
	In fact, a single server can handle multiple clients

	What is Node.js?

	Nodejs is a runtime environment which allows us to create/develop backend/server-side applications with JavaScript. Because by default, JS was conceptualized solely to the front end.
	Runtime Environment - is the environment in which a program or application is executed

	Why is NodeJS Popular?

	Performance - NodeJS is one of the most performing environment for creating backend applications with JS

	Familiarity - Since NodeJs is built and uses JS as its language, it is very familiar for most developers

	NPM - Node Package Manager - is the largest registry for node packages
	Packages - are bits of programs, methods, functions, codes that greatly help in the development of an application


*/

const http = require("http");

//creates a variable "port" to store the port number
const port = 4000

const server = http.createServer((request,response)=>{


	if (request.url == '/greeting'){

		response.writeHead(200,{'Content-Type':'text/plain'})
		response.end(`Welcome to 248 2048 App`)

	}

	else if (request.url == '/homepage'){

		response.writeHead(200,{'Content-Type':'text/plain'})
		response.end(`This is the homepage!`)

	}

	//MA
	//create an else condition that all other routes will return a message of "Page Not Available"

	else {

		response.writeHead(404,{'Content-Type':'text/plain'})
		response.end(`404 Page Not Available`)

	}


})

server.listen(port)

console.log(`Server is now accessible at localhost:${port}`)