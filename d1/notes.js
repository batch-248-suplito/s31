let http = require("http");


http.createServer(function(request,response) {

	response.writeHead(200,{'Content-Type':'text/html'});

	response.end(`
	
	<h1>What is a client?</h1>

	<img src="https://madooei.github.io/cs421_sp20_homepage/assets/client-server-1.png" width="20%" height="10%">

	<p>A client is an application which creates requests for resources from a server. A client will trigger an action, in the web development context, through a URL and wait for the response of the server.</p>

	<h1>What is a server?</h1>

	<p>A server is able to host and deliver resources requested by a client. In fact, a single server can handle multiple clients.</p>

	<h1>What is Node.js?</h1>

	<p>Nodejs is a runtime environment which allows us to create/develop backend/server-side applications with Javascript. Because by default, Javascript was conceptualized solely to the front end.</p>

	<h1>Why is NodeJS popular?</h1>

	<ul>
		<li>Performance - NodeJS is one of the most performing environment for creating backend applications with JS.</li>

		<li>Familiarity - Since NodeJS is built and uses JS as its language, it is very familiar for most developers.</li>

		<li>NPM - Node Package Manager is the largest registry for node packages. Packages are bits of programs, methods, functions, codes that greatly help in the development of an application.</li>
	</ul>
	`);

} ).listen(8000);


console.log('Server is running at localhost:8000');
