//a. What directive is used by Node.js in loading the modules it needs?
//ANSWER: require();
//b. What Node.js module contains a method for server creation?
//ANSWER: http
//c. What is the method of the http object responsible for creating a server using Node.js?
//ANSWER: createServer();
//d. What method of the response object allows us to set status codes and content types?
//ANSWER: response.writeHead();
//e. Where will console.log() output its contents when run in Node.js?
//ANSWER: terminal
//f. What property of the request object contains the address' endpoint?
//ANSWER: request.url

const http = require("http");
const port = 3000;

const server = http.createServer((request, response) => {
    if (request.url == '/login') {
        response.writeHead(200, {'Content-Type':'text/plain'});
        response.end(`Welcome to Log In Page!`);
    }
    else {
        response.writeHead(404, {'Content-Type':'text/plain'});
        response.end(`404 This Page not found!`);
    }

})
server.listen(port);
console.log(`The server is successfully running localHost: ${port}`);